#!/bin/bash

# Функция для запуска службы
start() {
    echo "Starting the service..."
    # Запуск скрипта configure.py на Python
    python3 conf.py
    # Переход в каталог terraform
    cd ./terraform
    # Инициализация Terraform
    terraform init
    # Применение конфигурации Terraform с автоматическим подтверждением
    terraform apply -auto-approve
    # Возврат в исходный каталог
    cd ..
    # Чтение адресов из файла network_config.txt
    addr1=$(sed -n '3p' network_config.txt)
    addr2=$(sed -n '4p' network_config.txt)

    # Функция для проверки доступности по пингу
    check_ping() {
        local addr=$1
        # Цикл, который продолжает проверять доступность адреса, пока он не станет доступен
        while ! ping -c 1 -W 1 "$addr" > /dev/null 2>&1; do
            echo "Waiting for $addr to be reachable..."
            sleep 5
        done
        echo "$addr is reachable."
    }

    # Проверка доступности по пингу для обоих адресов
    check_ping "$addr1"
    check_ping "$addr2"

    # Продолжение выполнения скрипта после успешного пинга
    # Запуск ansible-playbook с использованием инвентарного файла inventory и плейбука playbook.yml
    ansible-playbook -i inventory playbook.yml
    curl -o /dev/null -s -w "%{url_effective} %{http_code}\n" "$addr1"
    curl -o /dev/null -s -w "%{url_effective} %{http_code}\n" "$addr2"
}

# Функция для остановки службы
stop() {
    echo "Stopping the service..."
    # Переход в каталог terraform
    cd ./terraform
    # Уничтожение конфигурации Terraform с автоматическим подтверждением
    terraform destroy -auto-approve
    # Возврат в исходный каталог
    cd ..
    # Удаление inventory файла, конфигурационного файла сети и переменных Terraform
    rm inventory
    rm network_config.txt
    rm ./terraform/terraform.tfvars
}

# Функция для обновления зависимостей
update() {
    echo "Updating the requirements..."
    # Установка зависимостей из файла requirements.txt
    pip install -r requirements.txt
}

# Проверка наличия параметра командной строки
if [ -z "$1" ]; then
    echo "Usage: $0 {start|stop|update}"
    exit 1
fi

# Выполнение соответствующей функции в зависимости от параметра
case "$1" in
    start)
        start
        ;;
    stop)
        stop
        ;;
    update)
        update
        ;;
    *)
        echo "Usage: $0 {start|stop|update}"
        exit 1
        ;;
esac

# Завершение выполнения скрипта с кодом выхода 0
exit 0
