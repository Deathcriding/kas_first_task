# Файл с описанием переменных в конфигурации

# Префикс для создаваемых объектов
variable "prefix" {
  type    = string
  default = "web-"
}


# Параметры облачного образа
variable "image" {
  type = object({
    name = string
    path = string
  })
}

# Информация об используемом сетевом интерфейсе
variable "net" {
  type    = string
  default = "virbr0"
}

# Информация о создаваемых виртуальных машинах
variable "domains" {
  description = "List of VMs with specified parameters"
  type        = list(object({
    name   = string,
    cpu    = number,
    ram    = number,
  }))
}

# Описание переменной, в которой будет содержаться открытый ключ
variable "pub_rsa_key" {
  description = "Public RSA key for SSH access"
  type        = string
}
