# Конфигурация выводимой после создания виртуальных машин информации, содержащей название и адрес машин
output "vms_info" {
  description = "General information about created VMs"
  value = [
    for vm in libvirt_domain.vm : {   # Итерация по машинам
      id = vm.name    # Вывод названия машины
      ip = vm.network_interface[0].addresses.0    # Вывод адреса машины
    }
  ]
}