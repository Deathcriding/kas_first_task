# Описание используемого провайдера
provider "libvirt" {
  uri = "qemu:///system"
}

# Чтение IP-адресов и сети из файла
data "local_file" "network_config" {
  filename = "${path.module}/../network_config.txt"
}

# Создание переменных среды на основании файла network_config
locals {
  network_config_lines = split("\n", trimspace(data.local_file.network_config.content))
  gateway = local.network_config_lines[0]
  netmask_cidr = tonumber(local.network_config_lines[1])
  ip_list = slice(local.network_config_lines, 2, length(local.network_config_lines))

}

# Создание виртуальных дисков для каждой виртуальной машины
resource "libvirt_volume" "root" {
  count  = length(var.domains)
  name   = "${var.prefix}${var.domains[count.index].name}-root"
  source = var.image.path
}
# Создание самих виртуальных машин
resource "libvirt_domain" "vm" {
  count  = length(var.domains)
  name   = "${var.prefix}${var.domains[count.index].name}"
  vcpu   = var.domains[count.index].cpu
  memory = var.domains[count.index].ram

  qemu_agent = true
  autostart  = true

  cloudinit = libvirt_cloudinit_disk.commoninit[count.index].id

  network_interface {
    bridge         = var.net
    wait_for_lease = false
    addresses      = [local.ip_list[count.index]]
  }
  disk {
    volume_id = libvirt_volume.root[count.index].id
  }
}
# Шаблон для cloud-init user-data
data "template_file" "user_data" {
  count    = length(var.domains)
  template = file("${path.module}/cloud_init.cfg")
  vars = {
    pub_rsa_key  = var.pub_rsa_key
    ip_address   = local.ip_list[count.index]
    gateway      = local.gateway
    netmask_cidr = local.netmask_cidr
  }
}
# Создание дисков cloud-init для каждой виртуальной машины
resource "libvirt_cloudinit_disk" "commoninit" {
  count     = length(var.domains)
  name      = "commoninit-${count.index}.iso"
  user_data = data.template_file.user_data[count.index].rendered
}


