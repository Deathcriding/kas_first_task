import netifaces as ni
import ipaddress
import subprocess
from os import getenv
from dotenv import load_dotenv


load_dotenv()

with open('terraform/template_tfvars') as file:
    with open('terraform/terraform.tfvars', 'w') as out:
        print(file.read().replace('KVM_INTERFACE', getenv('KVM_INTERFACE')).replace('PUB_RSA_KEY',
                                                                              getenv('PUB_RSA_KEY')).replace(
            'PATH_TO_IMAGE', getenv('PATH_TO_IMAGE')).replace('IMAGE_NAME', getenv('IMAGE_NAME')), file=out)


def get_network_settings(interface):
    try:

        ip = ni.ifaddresses(interface)[ni.AF_INET][0]['addr']
        print(ip)
        netmask_cidr = sum(
            bin(int(x)).count('1') for x in ni.ifaddresses(interface)[ni.AF_INET][0]['netmask'].split('.'))
        return ip, netmask_cidr
    except Exception as e:
        print("Error:", e)
        return None, None


def generate_ip_addresses(ip_address, netmask_cidr, count):
    try:
        network = ipaddress.ip_network(f"{ip_address}/{netmask_cidr}", strict=False)
        ip_addresses = [str(ip) for ip in network.hosts()]
        available_ips = []

        for ip in ip_addresses:
            if len(available_ips) >= count:
                break
            if not is_ip_in_use(ip):
                available_ips.append(ip)

        return available_ips
    except Exception as e:
        print("Error:", e)
        return []


def is_ip_in_use(ip):
    try:
        result = subprocess.run(['ping', '-c', '1', '-W', '1', ip], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        return result.returncode == 0
    except Exception as e:
        print("Error during ping:", e)
        return True


# Пример использования:
interface_name = getenv("KVM_INTERFACE")
ip_address, netmask_cidr = get_network_settings(interface_name)

if ip_address and netmask_cidr:
    print("IP Address:", ip_address)
    print("Subnet Mask (CIDR notation):", netmask_cidr)
    total_count = 2

    ip_addresses = generate_ip_addresses(ip_address, netmask_cidr, total_count)

    if ip_addresses:
        print(f"Generated {len(ip_addresses)} IP addresses:")
        with open("network_config.txt", "w") as file:
            file.write(f"{ip_address}\n")
            file.write(f"{netmask_cidr}\n")
            for ip in ip_addresses:
                file.write(ip + "\n")
                print(ip)

        with open('inventory','w') as file:
            print('[web_servers]', file=file)
            print(f'vm1 ansible_host={ip_addresses[0]} ansible_user=root host_type=docker ansible_ssh_private_key_file={getenv("PATH_TO_SSH_PRIVATE_KEY")}', file=file)
            print(f'vm2 ansible_host={ip_addresses[1]} ansible_user=root host_type=virtual ansible_ssh_private_key_file={getenv("PATH_TO_SSH_PRIVATE_KEY")}', file=file)
    else:
        print("Failed to generate IP addresses.")
else:
    print("Failed to retrieve network settings.")
