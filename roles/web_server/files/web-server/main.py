import os
import sys
import atexit
from http.server import HTTPServer, BaseHTTPRequestHandler
from prometheus_client import start_http_server, Gauge, Counter
import subprocess
import signal

# Установка порта для основного HTTP-сервера
port = 80
# Указание пути к PID-файлу, который будет хранить идентификатор процесса сервера
pid_file = '/tmp/python_server.pid'

# Определение метрики Prometheus для типа хоста
host_type_metric = Gauge('host_type', 'Type of host where the server is running', ['host_type'])

# Определение метрики Prometheus для количества HTTP-кодов ответов
response_code_metric = Counter('http_response_codes', 'Number of HTTP response codes', ['response_code'])

def log_message(format, *args):
    # Получение кода ответа из аргументов
    code = args[1]
    # Увеличение счетчика метрик для соответствующего кода ответа
    response_code_metric.labels(code).inc()

def check_system():
    # Проверка, выполняется ли процесс в контейнере Docker
    if os.path.exists('/.dockerenv'):
        return 'Docker container'
    else:
        # Запуск команды dmidecode для получения информации о системе
        result = subprocess.run(r"dmidecode -t system | grep 'Manufacturer\|Product'", capture_output=True, shell=True, text=True)
        # Обработка и возврат результата команды
        return result.stdout.rstrip().replace('\t', '').replace('\n', '\t')

# Получение информации о типе хоста и установка соответствующей метрики
host_type = check_system()
host_type_metric.labels(host_type).set(1)

class CustomHTTPRequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        try:
            # Открытие файла index.html и чтение его содержимого
            with open('index.html', "rb") as file:
                content = file.read()

            # Отправка HTTP-ответа с кодом 200 и содержимым файла
            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            self.wfile.write(content)
        except FileNotFoundError:
            # Отправка HTTP-ответа с кодом 404, если файл не найден
            self.send_error(404, 'File Not Found: index.html')

def run_server(server_class=HTTPServer, handler_class=CustomHTTPRequestHandler, port=port):
    # Запуск сервера для метрик Prometheus на порту 8080
    start_http_server(8080)

    # Настройка адреса и порта для HTTP-сервера
    server_address = ('', port)
    # Создание экземпляра HTTP-сервера
    httpd = server_class(server_address, handler_class)
    # Переопределение метода log_message для использования метрик
    httpd.RequestHandlerClass.log_message = log_message
    print(f'Starting server on port {port}...')
    try:
        # Запуск HTTP-сервера
        httpd.serve_forever()
    except KeyboardInterrupt:
        # Остановка сервера при получении сигнала прерывания (Ctrl+C)
        httpd.server_close()
        print('Server stopped.')

def daemonize():
    # Создание фонового процесса
    if os.fork():
        sys.exit()

    # Создание новой сессии и отвод процесса
    os.setsid()

    # Вторая форк-операция для окончательного отделения фонового процесса
    if os.fork():
        sys.exit()

    # Перенаправление стандартных потоков ввода/вывода в /dev/null
    sys.stdout.flush()
    sys.stderr.flush()

    with open('/dev/null', 'r') as f:
        os.dup2(f.fileno(), sys.stdin.fileno())
    with open('/dev/null', 'a+') as f:
        os.dup2(f.fileno(), sys.stdout.fileno())
        os.dup2(f.fileno(), sys.stderr.fileno())

    # Запись PID текущего процесса в PID-файл
    with open(pid_file, 'w') as f:
        f.write(str(os.getpid()))

    # Регистрация функции удаления PID-файла при завершении процесса
    atexit.register(lambda: os.remove(pid_file))

def start():
    # Проверка, запущен ли уже сервер
    if os.path.exists(pid_file):
        print("Server is already running. Stopping old server...")
        with open(pid_file, 'r') as f:
            pid = int(f.read())
        # Остановка старого процесса сервера
        os.kill(pid, signal.SIGTERM)
        os.remove(pid_file)
        print("Old server stopped.")

    # Демонизация процесса и запуск сервера
    daemonize()
    run_server()

def stop():
    # Проверка, запущен ли сервер
    if not os.path.exists(pid_file):
        print("Server is not running.")
        sys.exit(1)

    # Чтение PID процесса из PID-файла
    with open(pid_file, 'r') as f:
        pid = int(f.read())

    # Остановка процесса сервера
    os.kill(pid, signal.SIGTERM)
    os.remove(pid_file)
    print("Server stopped.")

if __name__ == "__main__":
    # Обработка аргументов командной строки для запуска или остановки сервера
    if len(sys.argv) != 2:
        print("Usage: python3 main.py start|stop")
        sys.exit(1)

    if sys.argv[1] == 'start':
        start()
    elif sys.argv[1] == 'stop':
        stop()
    else:
        print("Unknown command.")
        sys.exit(1)
